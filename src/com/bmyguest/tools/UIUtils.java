package com.bmyguest.tools;

import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Window;

/**
 * Created by fofofofodev on 25/07/2017.
 */
public class UIUtils {


    public static void handleEmptyRows(MouseEvent event, TableView tableView) {
        ((Node) event.getSource()).getScene().addEventFilter(MouseEvent.MOUSE_CLICKED, evt -> {
            Node source = evt.getPickResult().getIntersectedNode();
            while (source != null && !(source instanceof TableRow)) {
                source = source.getParent();
            }
            // clear selection on click anywhere but on a filled row
            if (source == null || (source instanceof TableRow && ((TableRow) source).isEmpty())) {
                tableView.getSelectionModel().clearSelection();
            }
        });
    }

    public static void handleEmptyListCells(MouseEvent event, ListView list){
        ((Node) event.getSource()).getScene().addEventFilter(MouseEvent.MOUSE_CLICKED, evt -> {
            Node source = evt.getPickResult().getIntersectedNode();
            while (source != null && !(source instanceof TableRow)) {
                source = source.getParent();
            }
            if (source == null || (source instanceof TableRow && ((TableRow) source).isEmpty())) {
                list.getSelectionModel().clearSelection();
            }
        });
    }
}
