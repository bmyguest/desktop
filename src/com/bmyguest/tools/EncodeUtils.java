package com.bmyguest.tools;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

/**
 * Created by fofofofodev on 23/07/2017.
 */
public class EncodeUtils {

    public static String decodeBase64String(String str) {
        String decodedString = "";
        decodedString = new String(Base64.getDecoder().decode(str));
        return decodedString;
    }

}
