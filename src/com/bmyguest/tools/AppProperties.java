package com.bmyguest.tools;

import java.util.ResourceBundle;

/**
 * Created by fofofofodev on 12/07/2017.
 */
public class AppProperties {

    public static final ResourceBundle RB = ResourceBundle.getBundle("app_properties");
    public static final String API_URl_BASE = RB.getString("api_url_base");
    public static final String API_URL_SUFFIX_GET_EVENTS = RB.getString("api_url_suffix_get_events");
    public static final String CHARSET = RB.getString("charset");
    public static final String CONTENT_TYPE = RB.getString("content_type");
    public static final String FXML_HOME = RB.getString("fxml_home");
    public static final String FXML_MANAGE_EVENTS = RB.getString("fxm_manage_events");
    public static final String API_TOKEN = RB.getString("api_token");

    public static final String API_URL_SUFFIX_POST_USER = RB.getString("api_url_suffix_post_user");
    public static final String API_UR_SUFFIX_POST_EVENT = RB.getString("api_url_suffix_post_event");


    public static final String IMG_PARTY_ICON_URL = RB.getString("party_icon");
    public static final String IMG_USERS_ICON_URL = RB.getString("users_icon");
    public static final String IMG_MEDIAS_ICON_URL = RB.getString("medias_icon");
    public static final String IMG_LOCATION_ICON_URL = RB.getString("location_icon");
    public static final String IMG_CROWD_ICON_URL = RB.getString("crowd_icon");
    public static final String IMG_LOGOUT_ICON_URL = RB.getString("logout_icon");


    public static final String DEFAULT_LOGIN_EMAIL = RB.getString("login_email");
    public static final String DEAULT_LOGIN_PASSWORD = RB.getString("login_password");

    public static final String API_URL_DELETE_CONTACT=RB.getString("api_url_suffix_delete_contact_middle");
    public static final String API_URL_BASE_GROUP= RB.getString("api_url_base_group");
    public static final String API_URL_BASE_MEDIAS = RB.getString("api_url_suffix_media");

    public static final String API_URL_BASE_USERS = RB.getString("api_url_suffix_base_users");

    public static final String API_URL_BASE_PARTICIPANTS = RB.getString("api_url_base_participant");
    public static final String API_URL_BASE_EVALUATIONS = RB.getString("api_url_base_evaluations");

}
