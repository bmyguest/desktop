package com.bmyguest.network;

import com.bmyguest.entity.*;
import com.bmyguest.tools.AppProperties;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.deploy.net.HttpRequest;
import javafx.scene.control.Alert;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by fofofofodev on 12/07/2017.
 */
public class CustomConnection {

    private HttpURLConnection httpURLConnection;
    private ObjectMapper mapper = new ObjectMapper();

    public HttpURLConnection setConnection(String urlSuffix) {
        try {
            HttpURLConnection result = (HttpURLConnection) new URL(AppProperties.API_URl_BASE + urlSuffix).openConnection();
            result.setRequestProperty("Accept-charset", AppProperties.CHARSET);
            result.setRequestProperty("Content-Type", AppProperties.CONTENT_TYPE);
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Event[] getEvents() {
        try {
            mapper = new ObjectMapper();
            Event[] events = mapper.readValue(new URL(AppProperties.API_URl_BASE + AppProperties.API_URL_SUFFIX_GET_EVENTS), Event[].class);
            return events;
        } catch (IOException e) {
            Logger.getLogger(CustomConnection.class.getName()).log(Level.SEVERE, String.valueOf(e));
        }
        return null;
    }

    public User[] getUsers(){
        httpURLConnection = this.setConnection(AppProperties.API_URL_BASE_USERS);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestProperty("Authorization", AppProperties.API_TOKEN);
        try {
            httpURLConnection.setRequestMethod("GET");
            mapper = new ObjectMapper();
            return mapper.readValue(httpURLConnection.getInputStream(), User[].class);
        } catch (IOException e) {
            e.printStackTrace();
            Logger.getLogger(CustomConnection.class.getName()).log(Level.SEVERE, String.valueOf(e));
        }
        return null;
    }

    public  User getUserById(User user){
        try {
            httpURLConnection = this.setConnection(AppProperties.API_URL_SUFFIX_POST_USER + user.getId());
            //httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestProperty("Authorization", AppProperties.API_TOKEN);
            httpURLConnection.connect();
            mapper = new ObjectMapper();
            return mapper.readValue(httpURLConnection.getInputStream(), User.class);
        } catch (IOException e) {
            Logger.getLogger(CustomConnection.class.getName()).log(Level.SEVERE, String.valueOf(e));
        }
        return null;
    }



    public Event getEventById(Event event){
        try {
            httpURLConnection = this.setConnection(AppProperties.API_URL_SUFFIX_POST_USER + event.getId());
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestProperty("Authorization", AppProperties.API_TOKEN);
            httpURLConnection.connect();
            mapper = new ObjectMapper();
            return mapper.readValue(httpURLConnection.getInputStream(), Event.class);
        } catch (IOException e) {
            Logger.getLogger(CustomConnection.class.getName()).log(Level.SEVERE, String.valueOf(e));
        }
        return null;}

    public void deleteUSerGroup(Group userGroup){

        httpURLConnection = this.setConnection(AppProperties.API_URL_BASE_GROUP+userGroup.getId());
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestProperty("Authorization", AppProperties.API_TOKEN);
        try {
            httpURLConnection.setRequestMethod("DELETE");
            String jsonifiedEvent = mapper.writeValueAsString(userGroup);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
            outputStreamWriter.write(jsonifiedEvent);
            outputStreamWriter.flush();
            outputStreamWriter.close();

            httpURLConnection.connect();

            //Object object = mapper.readValue(httpURLConnection.getInputStream(), Object.class);
           // System.out.println(String.valueOf("String value of returned Object : " + object));

            BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            String temp = null;
            StringBuilder sb = new StringBuilder();
            while((temp = in.readLine()) != null){
                sb.append(temp).append(" ");
            }

            System.out.println(sb.toString());

        } catch (IOException e) {
            Logger.getLogger(CustomConnection.class.getName()).log(Level.SEVERE, String.valueOf(e));
        }

    }

    public Object deleteParticipant(Event event, User participant){
        httpURLConnection = this.setConnection(AppProperties.API_URL_SUFFIX_GET_EVENTS + "/" + event.getId() + AppProperties.API_URL_BASE_PARTICIPANTS + participant.getId());
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestProperty("Authorization", AppProperties.API_TOKEN);
        try {

            httpURLConnection.setRequestMethod("DELETE");
            String jsonifiedEvent = mapper.writeValueAsString(event);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
            outputStreamWriter.write(jsonifiedEvent);
            outputStreamWriter.flush();
            outputStreamWriter.close();
            httpURLConnection.connect();

            Object object = mapper.readValue(httpURLConnection.getInputStream(), Object.class);
            System.out.println(String.valueOf("String value of returned Object : " + object));
            return object;

        }catch (IOException e){
            Logger.getLogger(CustomConnection.class.getName()).log(Level.SEVERE, String.valueOf(e));
        }
        return null;
    }


    public Object deleteEvaluation(Event event, Evaluation evaluation){

        httpURLConnection = this.setConnection(AppProperties.API_URL_SUFFIX_GET_EVENTS + "/" + event.getId() + AppProperties.API_URL_BASE_EVALUATIONS + evaluation.getId());
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestProperty("Authorization", AppProperties.API_TOKEN);

        try{
            httpURLConnection.setRequestMethod("DELETE");
            String jsonifiedEvent = mapper.writeValueAsString(event);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
            outputStreamWriter.write(jsonifiedEvent);
            outputStreamWriter.flush();
            outputStreamWriter.close();
            httpURLConnection.connect();
            Object object = mapper.readValue(httpURLConnection.getInputStream(), Object.class);
            System.out.println(String.valueOf("String value of returned Object : " + object));
            return object;
        }catch (IOException e){
            Logger.getLogger(CustomConnection.class.getName()).log(Level.SEVERE, String.valueOf(e));
        }
        return null;
    }


    public Object deleteEvent(Event event) {
        httpURLConnection = this.setConnection(AppProperties.API_URL_SUFFIX_GET_EVENTS + "/" + event.getId());
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestProperty("Authorization", AppProperties.API_TOKEN);

        try{

            httpURLConnection.setRequestMethod("DELETE");
            String jsonifiedEvent = mapper.writeValueAsString(event);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
            outputStreamWriter.write(jsonifiedEvent);
            outputStreamWriter.flush();
            outputStreamWriter.close();
            httpURLConnection.connect();

            Object object = mapper.readValue(httpURLConnection.getInputStream(), Object.class);
            System.out.println(String.valueOf("String value of returned Object : " + object));
            return object;

        }catch (IOException e){
            Logger.getLogger(CustomConnection.class.getName()).log(Level.SEVERE, String.valueOf(e));
        }
        return null;
    }

    public void deleteUser(User user){

        httpURLConnection = this.setConnection(AppProperties.API_URL_SUFFIX_POST_USER + user.getId());
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestProperty("Authorization", AppProperties.API_TOKEN);
        try {

            httpURLConnection.setRequestMethod("DELETE");
            String jsonifiedEvent = mapper.writeValueAsString(user);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
            outputStreamWriter.write(jsonifiedEvent);
            outputStreamWriter.flush();
            outputStreamWriter.close();
            httpURLConnection.connect();

            Object object = mapper.readValue(httpURLConnection.getInputStream(), Object.class);
            System.out.println(String.valueOf("String value of returned Object : " + object));

        }catch (IOException e){
            Logger.getLogger(CustomConnection.class.getName()).log(Level.SEVERE, String.valueOf(e));
        }
    }

    public void deleteUserMedia(User user, Media userMedia) {

        httpURLConnection = this.setConnection(AppProperties.API_URL_SUFFIX_POST_USER + user.getId() + AppProperties.API_URL_BASE_MEDIAS + userMedia.getId());
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestProperty("Authorization", AppProperties.API_TOKEN);

        try {

            httpURLConnection.setRequestMethod("DELETE");
            String jsonifiedEvent = mapper.writeValueAsString(userMedia);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
            outputStreamWriter.write(jsonifiedEvent);
            outputStreamWriter.flush();
            outputStreamWriter.close();
            httpURLConnection.connect();

            //Object object = mapper.readValue(httpURLConnection.getInputStream(), Object.class);
            // System.out.println(String.valueOf("String value of returned Object : " + object));

            httpURLConnection.connect();
            BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            String temp = null;
            StringBuilder sb = new StringBuilder();
            while((temp = in.readLine()) != null){
                sb.append(temp).append(" ");
            }

            System.out.println(sb.toString());

        } catch (IOException e) {
            Logger.getLogger(CustomConnection.class.getName()).log(Level.SEVERE, String.valueOf(e));
        }

    }


    public void deleteUserContact(User user, User contact, String urlSuffix){

        httpURLConnection = this.setConnection(AppProperties.API_URL_SUFFIX_POST_USER + user.getId() + urlSuffix + contact.getId());
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestProperty("Authorization", AppProperties.API_TOKEN);
        try {
            httpURLConnection.setRequestMethod("DELETE");
            String jsonifiedEvent = mapper.writeValueAsString(contact);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
            outputStreamWriter.write(jsonifiedEvent);
            outputStreamWriter.flush();
            outputStreamWriter.close();
            httpURLConnection.connect();

            //Object object = mapper.readValue(httpURLConnection.getInputStream(), Object.class);
           // System.out.println(String.valueOf("String value of returned Object : " + object));

            httpURLConnection.connect();
            BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            String temp = null;
            StringBuilder sb = new StringBuilder();
            while((temp = in.readLine()) != null){
                sb.append(temp).append(" ");
            }

            System.out.println(sb.toString());

        } catch (IOException e) {
            Logger.getLogger(CustomConnection.class.getName()).log(Level.SEVERE, String.valueOf(e));
        }

    }

    public void insertUser(String urlSuffix) {

        Event event = new Event();
        event.setName("Souloukh Mbalakh");
        event.setDescription("KAY GNOU SOULOUKH MBALAKH BOU BARI WAAAAYAYYYYY");
        event.setCapacity(200);
        event.setVisibility(1);
        event.setStart(new Date());
        event.setEnd(new Date());
        event.setLatitude(3.4984904849);
        event.setLongitude(2.98398732832);

        try {

            httpURLConnection = this.setConnection(urlSuffix);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestProperty("Authorization", "847394834983765475");
            httpURLConnection.setRequestMethod("POST");

            String object = mapper.writeValueAsString(event);

            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
            outputStreamWriter.write(object);
            outputStreamWriter.flush();



            if (httpURLConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Logger.getLogger(CustomConnection.class.getName()).log(Level.SEVERE, String.valueOf(httpURLConnection.getErrorStream()));
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("REQUEST ERROR");
                alert.setHeaderText("Information Alert");
                alert.setContentText(String.valueOf(httpURLConnection.getResponseMessage()));
                alert.show();
            }

        } catch (IOException e) {
            e.printStackTrace();
            Logger.getLogger(CustomConnection.class.getName()).log(Level.SEVERE, String.valueOf(e));
        }
    }

}
