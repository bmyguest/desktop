package com.bmyguest.entity;

import com.bmyguest.entity.subEntities.UserHasContact;
import com.bmyguest.entity.subEntities.UserHasEvent;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by fofofofodev on 12/07/2017.
 */
public class User {

    private String id;
    private String name;
    private String email;
    private String birthday;
    private String gender;
    private String location;
    private boolean isPremium;
    private boolean isAdmin;
    private String picture;
    @JsonIgnore private String token;


    private ArrayList<Tag> tags = new ArrayList<>();
    private ArrayList<Evaluation> evaluations = new ArrayList<>();
    private ArrayList<Media> medias = new ArrayList<>();
    private ArrayList<User> contacts = new ArrayList<>();
    private ArrayList<Event> events = new ArrayList<>();
    private ArrayList<Group> groups = new ArrayList<>();
    private ArrayList<Group> memberships = new ArrayList<>();
    private ArrayList<Event> participations = new ArrayList<>();
    @JsonSerialize
    @JsonDeserialize
    @JsonIgnore private UserHasEvent eventsparticipants;
    private UserHasContact userscontact;


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public void setPremium(boolean premium) {
        isPremium = premium;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public void setTags(ArrayList<Tag> tags) {
        this.tags = tags;
    }

    public ArrayList<Evaluation> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(ArrayList<Evaluation> evaluations) {
        this.evaluations = evaluations;
    }

    public ArrayList<Media> getMedias() {
        return medias;
    }

    public void setMedias(ArrayList<Media> medias) {
        this.medias = medias;
    }

    public ArrayList<User> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<User> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Event> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<Event> events) {
        this.events = events;
    }

    private Map<String, Object> unrecognizedFields = new HashMap<>();

    @JsonAnyGetter
    public Map<String,Object> getUnrecognizedFields(String key, String value){
        return this.unrecognizedFields;
    }

    @JsonAnySetter
    public void setUnrecognizedFields(String key, String value){
        this.unrecognizedFields.put(key, value);
    }


    @JsonProperty(value="isAdmin")
    public boolean isAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserHasEvent getEventsparticipants() {
        return eventsparticipants;
    }

    public void setEventsparticipants(UserHasEvent eventsparticipants) {
        this.eventsparticipants = eventsparticipants;
    }


    public ArrayList<Group> getGroups() {
        return groups;
    }

    public void setGroups(ArrayList<Group> groups) {
        this.groups = groups;
    }

    public UserHasContact getUserscontact() {
        return userscontact;
    }

    public void setUserscontact(UserHasContact userscontact) {
        this.userscontact = userscontact;
    }

    public ArrayList<Group> getMemberships() {
        return memberships;
    }
    public void setMemberships(ArrayList<Group> memberships) {
        this.memberships = memberships;
    }


    public ArrayList<Event> getParticipations() {
        return participations;
    }

    public void setParticipations(ArrayList<Event> participations) {
        this.participations = participations;
    }
}
