package com.bmyguest.entity;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by fofofofodev on 12/07/2017.
 */
public class Tag {

    private int id;
    private String name;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private Tag eventstag;

    private Map<String, Object> unrecognizedFields = new HashMap<>();

    @JsonAnyGetter
    public Map<String,Object> getUnrecognizedFields(String key, String value){
        return this.unrecognizedFields;
    }

    @JsonAnySetter
    public void setUnrecognizedFields(String key, String value){
        this.unrecognizedFields.put(key, value);
    }

    public Tag getEventstag() {
        return eventstag;
    }

    public void setEventstag(Tag eventstag) {
        this.eventstag = eventstag;
    }
}
