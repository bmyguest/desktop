package com.bmyguest.entity;

import com.bmyguest.entity.subEntities.UserHasEvent;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by fofofofodev on 12/07/2017.
 */
public class Event {

    private int id;
    private String name;
    private String description;
    private Date start = new Date();
    private Date end = new Date();
    private double latitude;
    private double longitude;
    private int capacity;
    private int recurrence;
    private int visibility;

    @JsonSerialize
    @JsonDeserialize
    @JsonIgnore private UserHasEvent eventsparticipants;



    @JsonSerialize
    @JsonDeserialize
    private Picture picture;

    @JsonSerialize
    @JsonDeserialize
    private User owner;

    @JsonSerialize
    @JsonDeserialize
    private List<User> participants = new ArrayList<>();

    @JsonSerialize
    @JsonDeserialize
    @JsonUnwrapped
    private List<Media> medias = new ArrayList<>();

    @JsonSerialize
    @JsonDeserialize
    private List<Evaluation> evaluations = new ArrayList<>();

    @JsonUnwrapped
    private ArrayList<Tag> tags = new ArrayList<>();


    public Date getEnd() {
        return end;
    }

    public Date getStart() {
        return start;
    }

    public double getLatitude() {
        return latitude;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(int recurrence) {
        this.recurrence = recurrence;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    private Map<String, Object> unrecognizedFields = new HashMap<>();

    @JsonAnyGetter
    public Map<String,Object> getUnrecognizedFields(String key, String value){
        return this.unrecognizedFields;
    }

    @JsonAnySetter
    public void setUnrecognizedFields(String key, String value){
        this.unrecognizedFields.put(key, value);
    }


    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public List<User> getParticipants() {
        return participants;
    }

    public void setParticipants(List<User> participants) {
        this.participants = participants;
    }

    public List<Media> getMedias() {
        return medias;
    }

    public void setMedias(ArrayList<Media> medias) {
        this.medias = medias;
    }

    public List<Evaluation> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(List<Evaluation> evaluations) {
        this.evaluations = evaluations;
    }


    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public  Map<String,Object> getAdditionalProperties(){
        return additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(Map<String, Object> props){
        this.additionalProperties = props;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public void setTags(ArrayList<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return name + " - " + capacity + new SimpleDateFormat("dd/MM/yyyy").format(start);
    }

    public UserHasEvent getEventsparticipants() {
        return eventsparticipants;
    }

    public void setEventsparticipants(UserHasEvent eventsparticipants) {
        this.eventsparticipants = eventsparticipants;
    }
}
