package com.bmyguest.entity;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.css.SimpleStyleableStringProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by fofofofodev on 12/07/2017.
 */
public class Evaluation {

    private int id;
    private int note;
    private String comment;
    private User user;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    private Map<String, Object> unrecognizedFields = new HashMap<>();

    @JsonAnyGetter
    public Map<String,Object> getUnrecognizedFields(String key, String value){
        return this.unrecognizedFields;
    }

    @JsonAnySetter
    public void setUnrecognizedFields(String key, String value){
        this.unrecognizedFields.put(key, value);
    }


}
