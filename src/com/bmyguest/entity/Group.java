package com.bmyguest.entity;

import com.bmyguest.entity.subEntities.MemberHasGroup;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by fofofofodev on 23/07/2017.
 */
public class Group {
    private int id;
    private String name;
    private String description;
    private int open;
    private int visibility;
    private int pictureId;
    private String ownerId;
    private  Picture picture;

    private MemberHasGroup groupsmember;

    @JsonIgnore private String createdAt;
    @JsonIgnore private String updatedAt;
    @JsonIgnore private String deletedAt;



    private Map<String, Object> unrecognizedFields = new HashMap<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getOpen() {
        return open;
    }

    public void setOpen(int open) {
        this.open = open;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public int getPictureId() {
        return pictureId;
    }

    public void setPictureId(int pictureId) {
        this.pictureId = pictureId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Map<String, Object> getUnrecognizedFields() {
        return unrecognizedFields;
    }

    public void setUnrecognizedFields(Map<String, Object> unrecognizedFields) {
        this.unrecognizedFields = unrecognizedFields;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public MemberHasGroup getGroupsmember() {
        return groupsmember;
    }

    public void setGroupsmember(MemberHasGroup groupsmember) {
        this.groupsmember = groupsmember;
    }
}
