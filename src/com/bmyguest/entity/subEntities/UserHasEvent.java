package com.bmyguest.entity.subEntities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by fofofofodev on 21/07/2017.
 */
public class UserHasEvent {

    private int status;
    private int eventId;
    private String userId;
    @JsonIgnore private String createdAt;
    @JsonIgnore private String updatedAt;
    @JsonIgnore private String token;
    @JsonIgnore private String deletedAt;

    private Map<String, Object> unrecognizedFields = new HashMap<>();

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Map<String, Object> getUnrecognizedFields() {
        return unrecognizedFields;
    }

    public void setUnrecognizedFields(Map<String, Object> unrecognizedFields) {
        this.unrecognizedFields = unrecognizedFields;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
