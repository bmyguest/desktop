package com.bmyguest.entity.subEntities;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by fofofofodev on 21/07/2017.
 */
public class MediaHasEvent  {

    private int mediaId;
    private int eventId;

    @JsonIgnore private String createdAt;
    @JsonIgnore private String updatedAt;
    @JsonIgnore private String deletedAt;

    private Map<String, Object> unrecognizedFields = new HashMap<>();
    public int getMediaId() {
        return mediaId;
    }
    public void setMediaId(int mediaId) {
        this.mediaId = mediaId;
    }
    public int getEventId() {
        return eventId;
    }
    public void setEventId(int eventId) {
        this.eventId = eventId;
    }
    @JsonAnyGetter
    public Map<String, Object> getUnrecognizedFields() {
        return unrecognizedFields;
    }
    public void setUnrecognizedFields(Map<String, Object> unrecognizedFields) {
        this.unrecognizedFields = unrecognizedFields;
    }
    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }
}
