package com.bmyguest.entity;

import com.bmyguest.entity.subEntities.MediaHasEvent;
import com.bmyguest.entity.subEntities.MediaHasUser;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by fofofofodev on 12/07/2017.
 */

public class Media {
    private int id;
    private String file;
    private int visibility;
    private int categoryId;



    @JsonIgnore private String createdAt;
    @JsonIgnore private String updatedAt;
    @JsonIgnore private String deletedAt;
    @JsonSerialize
    @JsonDeserialize
    private MediaHasEvent eventsmedias;
    private MediaHasUser usersmedias;
    private Map<String, Object> unrecognizedFields = new HashMap<>();

    @JsonAnyGetter
    public Map<String,Object> getUnrecognizedFields(String key, String value){
        return this.getUnrecognizedFields();
    }

    @JsonAnySetter
    public void setUnrecognizedFields(String key, String value){
        this.getUnrecognizedFields().put(key, value);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public Map<String, Object> getUnrecognizedFields() {
        return unrecognizedFields;
    }

    public void setUnrecognizedFields(Map<String, Object> unrecognizedFields) {
        this.unrecognizedFields = unrecognizedFields;
    }
    public int getCategoryId() {
        return categoryId;
    }
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
    public MediaHasUser getUsersmedias() {
        return usersmedias;
    }
    public void setUsersmedias(MediaHasUser usersmedias) {
        this.usersmedias = usersmedias;
    }
}
