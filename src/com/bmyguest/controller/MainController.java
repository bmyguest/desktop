package com.bmyguest.controller;

import com.bmyguest.tools.AppProperties;
import com.jfoenix.effects.JFXDepthManager;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Window;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by fofofofodev on 21/07/2017.
 */
public class MainController implements Initializable {
    @FXML private Button menu_btns_events,menu_btns_users,menu_btns_medias, logout_btn;
    @FXML private AnchorPane manage_anchor_top, manage_current_tab_container;



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setBtnGraphic(menu_btns_events, AppProperties.IMG_PARTY_ICON_URL);
        setBtnGraphic(menu_btns_users,AppProperties.IMG_USERS_ICON_URL);
        setBtnGraphic(menu_btns_medias,AppProperties.IMG_MEDIAS_ICON_URL);
        setBtnGraphic(logout_btn,AppProperties.IMG_LOGOUT_ICON_URL);
        setCurrentView("../layout/manage_events.fxml");

        menu_btns_events.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setCurrentView("../layout/manage_events.fxml");
            }
        });

        menu_btns_users.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setCurrentView("../layout/manage_users.fxml");
            }
        });

        logout_btn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                logout(event);
            }
        });

        JFXDepthManager.setDepth(manage_anchor_top, 500);
    }

    public void setBtnGraphic(Button btn, String imgUrl) {
        Image img = new Image (getClass().getResourceAsStream(imgUrl));
        ImageView imageView = new ImageView(img);
        btn.setGraphic(imageView);
    }

    public void setCurrentView(String fxmlLayoutUrl){
        try {
            URL url  = getClass().getResource(fxmlLayoutUrl);
            FXMLLoader fxmlLoader  = new FXMLLoader();
            fxmlLoader.setLocation(url);
            fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
            AnchorPane pane  = fxmlLoader.load(url.openStream());
            manage_current_tab_container.getChildren().clear();
            manage_current_tab_container.getChildren().add(pane);
        } catch (IOException e) {
            e.printStackTrace();
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,String.valueOf(e));
        }
    }

    public void logout(MouseEvent event){
        Window window = ((Node) event.getSource()).getScene().getWindow();
        Parent homeScene = null;
        try {
            homeScene = FXMLLoader.load(getClass().getResource("../layout/index.fxml"));
            window.getScene().setRoot(homeScene);
        } catch (IOException e) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE,String.valueOf(e));
        }
    }



}
