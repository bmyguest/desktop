package com.bmyguest.controller;

import com.bmyguest.entity.Evaluation;
import com.bmyguest.entity.Event;
import com.bmyguest.entity.User;
import com.bmyguest.network.CustomConnection;
import com.bmyguest.tools.AppProperties;
import com.bmyguest.tools.EncodeUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

/**
 * Created by fofofofodev on 22/07/2017.
 */
public class ManageEvents  implements Initializable{

    public Event pickedEvent;


    @FXML private ListView list_view_events;
    ObservableList<Event> events;
    CustomConnection connection;


    @FXML private ImageView tab_pane_event_details_crowd_icon;
    @FXML private ImageView tab_pane_event_imgView;
    @FXML private Label label_event_place, label_event_start_date, label_event_end_date,label_event_owner_name, label_event_total_participants, label_event_description;
    @FXML private ImageView tab_pane_event_details_location_icon;

    @FXML private TableView table_view_particpants;
    @FXML TableColumn<User, String> table_column_participant_name, table_column_participant_location;
    @FXML TableColumn<User, User> table_column_participant_premium;
    ObservableList<User> participants_data;


    @FXML private TableView table_view_evaluations;
    @FXML private TableColumn<Evaluation, Integer> table_column_evaluation_note;
    @FXML private TableColumn<Evaluation, String> table_column_evaluation_comment;
    ObservableList<Evaluation> evaluations_data;


    @FXML private AnchorPane tab_pane_event_details;



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        connection = new CustomConnection();

        initEventsListView();
        if(list_view_events.getItems() != null){
            displayEvent(0);
        }
    }

    private void initEventsListView() {
        events = FXCollections.observableArrayList(connection.getEvents());
        if(events != null) {
            list_view_events.setItems(events);
            list_view_events.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

            ContextMenu contextMenu = new ContextMenu();
            contextMenu.setStyle("-fx-background-color:#724f3f;" +
                    "-fx-fill:white;" + "-fx-min-width:100px;" + "-fx-background-radius:50px;");

            MenuItem menuDeleteItem = new MenuItem();
            menuDeleteItem.setText("Delete");
            menuDeleteItem.setOnAction(event -> {
                Event clicked = (Event) events.get(list_view_events.getSelectionModel().getSelectedIndex() + 1);
                Object responseObject = connection.deleteEvent(pickedEvent);
                if(responseObject != null){

                    resetDetailPane();

                    list_view_events.getItems().remove(pickedEvent);
                    pickedEvent = null;
                    list_view_events.refresh();
                }
                contextMenu.hide();
            });

            contextMenu.getItems().addAll(menuDeleteItem);
            list_view_events.setCellFactory(param -> {
                return new ListCell<Event>() {
                    @Override
                    protected void updateItem(Event item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null && !empty) {
                            ImageView imageView = new ImageView("com/bmyguest/Images_Assets/web_hi_res_512.png");

                            if(item.getPicture() != null && item.getPicture().getFile() != null){
                                imageView = new ImageView(item.getPicture().getFile());
                            }

                            imageView.setFitWidth(60);
                            imageView.setFitHeight(40);
                            imageView.preserveRatioProperty();Label label = new Label(item.getName());
                            label.setTextFill(Color.WHITE);

                            this.setContextMenu(contextMenu);
                            HBox hBox = new HBox();
                            hBox.setSpacing(10);
                            hBox.getChildren().addAll(imageView, label);
                            hBox.setAlignment(Pos.CENTER_LEFT);
                            //setEditable(false);
                            setGraphic(hBox);
                        }
                    }
                };
            });
            list_view_events.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    Event clickedEvent = (Event) list_view_events.getSelectionModel().getSelectedItem();
                    list_view_events.setEditable(false);
                    if(clickedEvent != null){
                        if(clickedEvent.getPicture() != null && clickedEvent.getPicture().getFile() != null){
                            displayEvent(list_view_events.getSelectionModel().getSelectedIndex());
                            contextMenu.show(list_view_events, event.getScreenX(), event.getScreenY() + 20);
                        }
                    }else{
                        event.consume();
                    }
                    list_view_events.getSelectionModel().clearSelection();
                }
            });

        }
    }

    private void resetDetailPane() {

        tab_pane_event_details_location_icon.setImage(null);
        tab_pane_event_details_crowd_icon.setImage(null);
        tab_pane_event_imgView.setImage(null);
        label_event_place.setText(null);
        label_event_start_date.setText(null);
        label_event_end_date.setText(null);
        label_event_owner_name.setText(null);
        label_event_total_participants.setText(null);
        label_event_description.setText(null);

        table_view_particpants.getItems().clear();
        table_view_evaluations.getItems().clear();

    }

    public void displayEvent(int pos) {
        if(list_view_events.getItems() != null && list_view_events.getItems().size() > 0){

            pickedEvent = (pos == 1) ? (Event) list_view_events.getItems().get(1) : (Event) list_view_events.getItems().get(pos);
            label_event_owner_name.setText("By " + pickedEvent.getOwner().getName());
            label_event_start_date.setText(
                    "From " + new SimpleDateFormat("dd/MM/yyyy HH:mm").format(pickedEvent.getStart()).toString());
            label_event_end_date.setText(
                    "To " + new SimpleDateFormat("dd/MM/yyyy HH:mm").format(pickedEvent.getStart()).toString());
            label_event_description.setText(EncodeUtils.decodeBase64String(pickedEvent.getDescription()));
            label_event_place.setText(pickedEvent.getOwner().getLocation());
            label_event_total_participants.setText(String.valueOf(pickedEvent.getParticipants().size()));
            tab_pane_event_imgView.setImage(new Image(pickedEvent.getPicture().getFile()));

            tab_pane_event_details_crowd_icon.setImage(new Image("com/bmyguest/Images_Assets/crowd_icon.png"));
            tab_pane_event_details_location_icon.setImage(new Image("com/bmyguest/Images_Assets/location_icon.png"));

            handleParticipantsPane();
            handleEvaluationsPane();
        }
    }

    private void handleEvaluationsPane() {
        evaluations_data = FXCollections.observableList(pickedEvent.getEvaluations());
        table_view_evaluations.setItems(evaluations_data);
        table_column_evaluation_note.setCellFactory(new Callback<TableColumn<Evaluation, Integer>, TableCell<Evaluation, Integer>>() {
            @Override
            public TableCell<Evaluation, Integer> call(TableColumn<Evaluation, Integer> param) {
                TableCell<Evaluation, Integer> cell =  new TableCell<Evaluation, Integer>(){
                    @Override
                    protected void updateItem(Integer item, boolean empty) {
                        if(item != null && !empty){
                            Label label = new Label(String.valueOf(item));
                            HBox hBox = new HBox();
                            hBox.getChildren().addAll(label);
                            setPrefWidth(15);
                            setGraphic(hBox);
                        }
                    }
                };
                return cell;
            }
        });

        table_column_evaluation_comment.setCellFactory(new Callback<TableColumn<Evaluation, String>, TableCell<Evaluation, String>>() {
            @Override
            public TableCell<Evaluation, String> call(TableColumn<Evaluation, String> param) {
                TableCell<Evaluation, String> cell =  new TableCell<Evaluation, String>(){
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        if(item != null && !empty){
                            Label label = new Label(item);
                            label.setWrapText(true);
                            VBox vBox = new VBox();
                            vBox.getChildren().addAll(label);
                            setGraphic(vBox);
                        }
                    }
                };
                return cell;
            }
        });

        ContextMenu contextMenu = new ContextMenu();
        contextMenu.setStyle("-fx-background-color:#724f3f;" +
                "-fx-fill:white;" + "-fx-min-width:100px;" + "-fx-background-radius:50px;");

        MenuItem menuDeleteItem = new MenuItem();
        menuDeleteItem.setText("Delete");
        menuDeleteItem.setOnAction(e -> {
            Evaluation pickedEvaluation = evaluations_data.get(table_view_evaluations.getSelectionModel().getSelectedIndex() + 1);
            Object responseObject = connection.deleteEvaluation(pickedEvent, pickedEvaluation);
            //event = connection.getEventById(event);
            if(responseObject != null){
                evaluations_data.remove(pickedEvaluation);
            }
            //System.out.println(((Event)table_view_evaluations.getSelectionModel().getSelectedItem()).getName());
            contextMenu.hide();
        });

        contextMenu.getItems().addAll(menuDeleteItem);
        table_view_evaluations.setContextMenu(contextMenu);

        table_view_evaluations.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Evaluation clickedEval = (Evaluation) table_view_evaluations.getSelectionModel().getSelectedItem();
                if(clickedEval != null){
                    contextMenu.show(table_view_evaluations, event.getScreenX(), event.getScreenY());
                }
            }
        });
    }

    private void handleParticipantsPane() {
        participants_data = FXCollections.observableArrayList(pickedEvent.getParticipants());
        table_view_particpants.setItems(participants_data);
        table_view_particpants.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        table_column_participant_name.setCellValueFactory(new PropertyValueFactory<User, String>("name"));
        table_column_participant_name.setCellFactory(new Callback<TableColumn<User, String>, TableCell<User, String>>() {
            @Override
            public TableCell<User, String> call(TableColumn<User, String> param) {
                return new TableCell<User, String>(){
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        if(item != null && !empty){
                            Label label = new Label(item);
                            label.setWrapText(true);
                            label.setStyle("-fx-text-align:center");
                            VBox vBox = new VBox();
                            vBox.setStyle("-fx-alignment:center");
                            vBox.setSpacing(10);
                            vBox.getChildren().addAll(label);
                            setGraphic(vBox);
                        }
                    }
                };
            }
        });

        table_column_participant_location.setCellValueFactory(new PropertyValueFactory<User, String>("location"));
        table_column_participant_location.setCellFactory(new Callback<TableColumn<User, String>, TableCell<User, String>>() {
            @Override
            public TableCell<User, String> call(TableColumn<User, String> param) {
                return new TableCell<User, String>(){
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        if(item != null && !empty){
                            Label label = new Label(item);
                            label.setWrapText(true);
                            label.setStyle("-fx-text-align:center");
                            VBox vBox = new VBox();
                            vBox.setStyle("-fx-alignment:center");
                            vBox.setSpacing(10);
                            vBox.getChildren().addAll(label);
                            setGraphic(vBox);
                        }

                    }
                };
            }
        });
        table_column_participant_premium.setCellFactory(new Callback<TableColumn<User, User>, TableCell<User, User>>() {
            @Override
            public TableCell<User, User> call(TableColumn<User, User> param) {
                TableCell<User, User> cell = new TableCell<User, User>(){
                    @Override
                    protected void updateItem(User item, boolean empty) {
                        if(!empty){
                            CheckBox checkBox = new CheckBox();
                            if(item != null && item.isPremium()){
                                checkBox.setSelected(true);
                            }
                            checkBox.setSelected(true);
                            setEditable(false);
                            VBox vBox = new VBox();
                            vBox.setStyle("-fx-alignment:center");
                            vBox.setSpacing(10);
                            vBox.getChildren().addAll(checkBox);
                            setGraphic(vBox);
                        }
                    }
                };
                return cell;
            }
        });

        ContextMenu contextMenu = new ContextMenu();
        contextMenu.setStyle("-fx-background-color:#724f3f;" +
                "-fx-fill:white;" + "-fx-min-width:100px;" + "-fx-background-radius:50px;");

        MenuItem menuDeleteItem = new MenuItem();
        menuDeleteItem.setText("Delete");

        menuDeleteItem.setOnAction(e -> {
            User pickedParticipant = participants_data.get(table_view_particpants.getSelectionModel().getSelectedIndex() + 1);
            Object responseObject = connection.deleteParticipant(pickedEvent,pickedParticipant);
            if(responseObject != null){
                participants_data.remove(pickedParticipant);
            }
            contextMenu.hide();
        });

        contextMenu.getItems().addAll(menuDeleteItem);
        table_view_particpants.setContextMenu(contextMenu);
        table_view_particpants.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
               User clicked =  (User) table_view_particpants.getSelectionModel().getSelectedItem();
               if(clicked !=null){
                   contextMenu.show(table_view_particpants, event.getScreenX(), event.getScreenY());
               }else {
                   event.consume();
               }
               table_view_particpants.getSelectionModel().clearSelection();
            }
        });
    }

}
