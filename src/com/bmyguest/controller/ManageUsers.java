package com.bmyguest.controller;


import com.bmyguest.entity.Group;
import com.bmyguest.entity.Media;
import com.bmyguest.entity.User;
import com.bmyguest.network.CustomConnection;
import com.bmyguest.tools.AppProperties;
import com.bmyguest.tools.EncodeUtils;
import com.bmyguest.tools.UIUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by fofofofodev on 23/07/2017.
 */
public class ManageUsers implements Initializable {

    User user;
    User pickedContact;
    CustomConnection customConnection;

    @FXML private ListView list_view_users;
    ObservableList<User> users;

    @FXML private ListView list_view_user_medias;
    ObservableList<Media> medias;

    @FXML private TableView table_view_user_groups;
    ObservableList<Group> userGroups;
    @FXML private TableColumn<Group, String> table_view_user_group_name;
    @FXML private TableColumn<Group, String> table_view_user_group_description;


    @FXML private TableView table_view_user_contacts;
    ObservableList<User> userContacts;
    @FXML private TableColumn<User, String> table_column_contact_name;
    @FXML private TableColumn<User, String> table_column_contact_location;
    @FXML private TableColumn<User, User> table_column_contact_premium;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        customConnection = new CustomConnection();
        setUserListView();
        if(list_view_users.getItems() != null){
            displayUser(0);
        }

    }

    public void displayUser(int position){
        user = (position == 0) ? (User) list_view_users.getItems().get(0) : (User) list_view_users.getItems().get(position);
        setUserMediasListView();
        setUserGroupsTableView();
        setUserContactsTableView();
    }

    private void setUserContactsTableView() {
        userContacts = FXCollections.observableArrayList(user.getContacts());
        table_view_user_contacts.setItems(userContacts);

        table_column_contact_premium.setCellValueFactory(new PropertyValueFactory<User, User>("isPremium"));
        table_column_contact_premium.setCellFactory(new Callback<TableColumn<User, User>, TableCell<User, User>>() {
            @Override
            public TableCell<User, User> call(TableColumn<User, User> param) {
                TableCell<User, User> cell = new TableCell<User, User>(){
                    @Override
                    protected void updateItem(User item, boolean empty) {
                        if(!empty){
                            CheckBox checkBox = new CheckBox();
                            if(item != null && item.isPremium()){
                                checkBox.setSelected(true);
                            }
                            checkBox.setSelected(true);
                            setEditable(false);
                            VBox vBox = new VBox();
                            vBox.setStyle("-fx-alignment:center");
                            vBox.setSpacing(13);
                            vBox.getChildren().addAll(checkBox);
                            setGraphic(vBox);
                        }
                    }
                };

                return cell;
            }
        });

        table_column_contact_name.setCellValueFactory(new PropertyValueFactory<User, String>("name"));
        table_column_contact_name.setCellFactory(new Callback<TableColumn<User, String>, TableCell<User, String>>() {
            @Override
            public TableCell<User, String> call(TableColumn<User, String> param) {
                return new TableCell<User, String>(){
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        if(item != null && !empty){
                            Label label = new Label(item);
                            label.setWrapText(true);
                            label.setStyle("-fx-text-align:center");
                            VBox vBox = new VBox();
                            vBox.setStyle("-fx-alignment:center");
                            vBox.setSpacing(10);
                            vBox.getChildren().addAll(label);
                            setGraphic(vBox);
                        }
                    }
                };
            }
        });

        table_column_contact_location.setCellValueFactory(new PropertyValueFactory<User, String>("location"));
        table_column_contact_location.setCellFactory(new Callback<TableColumn<User, String>, TableCell<User, String>>() {
            @Override
            public TableCell<User, String> call(TableColumn<User, String> param) {
                return new TableCell<User, String>(){
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        if(item != null && !empty){
                            Label label = new Label(item);
                            label.setWrapText(true);
                            label.setStyle("-fx-text-align:center");
                            VBox vBox = new VBox();
                            vBox.setStyle("-fx-alignment:center");
                            vBox.setSpacing(10);
                            vBox.getChildren().addAll(label);
                            setGraphic(vBox);
                        }

                    }
                };
            }
        });

        ContextMenu contextMenu = new ContextMenu();
        contextMenu.setStyle("-fx-background-color:#724f3f;" +
                "-fx-fill:white;" + "-fx-min-width:100px;" + "-fx-background-radius:50px;");

        MenuItem menuDeleteItem = new MenuItem();
        menuDeleteItem.setText("Delete");
        menuDeleteItem.setOnAction(e -> {
            new CustomConnection().deleteUserContact(user, pickedContact, AppProperties.API_URL_DELETE_CONTACT);
            user = customConnection.getUserById(user);
            userContacts.remove(pickedContact);
            userContacts.clear();
            userContacts.addAll(user.getContacts());
            table_view_user_contacts.refresh();
            contextMenu.hide();
        });

        contextMenu.getItems().addAll(menuDeleteItem);
        table_view_user_contacts.setContextMenu(contextMenu);
        table_view_user_contacts.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                UIUtils.handleEmptyRows(event,table_view_user_contacts);
                pickedContact =  (User) table_view_user_contacts.getSelectionModel().getSelectedItem();
                if(pickedContact !=null){
                    contextMenu.show(table_view_user_contacts, event.getScreenX(), event.getScreenY());
                }
            }
        });
    }

    private void setUserGroupsTableView() {
        ContextMenu contextMenu = new ContextMenu();
        contextMenu.setStyle("-fx-background-color:#724f3f;" +
                "-fx-fill:white;" + "-fx-min-width:100px;" + "-fx-background-radius:50px;");
        MenuItem menuDeleteItem = new MenuItem();
        menuDeleteItem.setText("Delete");
        menuDeleteItem.setOnAction(event -> {
            Group pickedgroup = ((Group) table_view_user_groups.getSelectionModel().getSelectedItem());
            customConnection.deleteUSerGroup(pickedgroup);
            user = customConnection.getUserById(user);
            userGroups.remove(pickedgroup);
            table_view_user_groups.refresh();
            contextMenu.hide();
        });

        contextMenu.getItems().addAll(menuDeleteItem);
        table_view_user_groups.setContextMenu(contextMenu);

        userGroups = FXCollections.observableArrayList(user.getGroups());
        table_view_user_groups.setItems(userGroups);

        table_view_user_group_name.setCellValueFactory(new PropertyValueFactory<Group, String>("name"));
        table_view_user_group_name.setCellFactory(new Callback<TableColumn<Group, String>, TableCell<Group, String>>() {
            @Override
            public TableCell<Group, String> call(TableColumn<Group, String> param) {

                return new TableCell<Group, String>(){
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        if(item != null && !empty){
                            VBox vBox = new VBox();
                            Label label = new Label(item);
                            label.setWrapText(true);
                            vBox.setStyle("-fx-alignment:center");
                            vBox.setSpacing(10);
                            vBox.getChildren().addAll(label);
                            setGraphic(vBox);

                        }
                    }
                };
            }
        });
        table_view_user_group_description.setCellValueFactory(new PropertyValueFactory<Group, String>("description"));
        table_view_user_group_description.setCellFactory(new Callback<TableColumn<Group, String>, TableCell<Group, String>>() {
            @Override
            public TableCell<Group, String> call(TableColumn<Group, String> param) {

                return new TableCell<Group, String>(){
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        if(item != null && !empty){
                            VBox vBox = new VBox();
                            Label label = new Label(EncodeUtils.decodeBase64String(item));
                            //label.setEditable(false);
                            label.setStyle("-fx-text-alignment:center");
                            label.setWrapText(true);
                            vBox.setStyle("-fx-alignment:center");
                            vBox.setSpacing(10);
                            vBox.getChildren().addAll(label);
                            setGraphic(vBox);
                        }
                    }
                };
            }
        });

        table_view_user_groups.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                UIUtils.handleEmptyRows(event, table_view_user_groups);
                Group clickedgroup = (Group) table_view_user_groups.getSelectionModel().getSelectedItem();
                if(clickedgroup != null){
                    contextMenu.show(table_view_user_groups, event.getScreenX(), event.getScreenY());
                }
            }
        });

    }

    private void setUserMediasListView() {
        medias = FXCollections.observableArrayList(user.getMedias());

        if(medias != null) {
            list_view_user_medias.setItems(medias);
            ContextMenu contextMenu = new ContextMenu();
            contextMenu.setStyle("-fx-background-color:#724f3f;" +
                    "-fx-fill:white;" + "-fx-min-width:100px;" + "-fx-background-radius:50px;");
            MenuItem menuDeleteItem = new MenuItem();
            menuDeleteItem.setText("Delete");

            list_view_user_medias.setCellFactory(param -> {
                return new ListCell<Media>(){
                    @Override
                    protected void updateItem(Media item, boolean empty) {
                        if(item != null && !empty){

                            ImageView imageView = new ImageView(item.getFile());
                            imageView.setFitWidth(350);
                            imageView.setFitHeight(350);
                            imageView.preserveRatioProperty();
                            imageView.setSmooth(true);
                            imageView.setCache(true);

                            VBox vBox = new VBox();
                            vBox.getChildren().addAll(imageView);
                            setGraphic(vBox);
                        }
                    }
                };
            });

            menuDeleteItem.setOnAction(event -> {
                Media pickedMedia = medias.get(list_view_user_medias.getSelectionModel().getSelectedIndex() + 1);
                customConnection.deleteUserMedia(user,pickedMedia);
                list_view_user_medias.getItems().remove(pickedMedia);
                user = customConnection.getUserById(user);
                list_view_user_medias.refresh();
                contextMenu.hide();
            });

            contextMenu.getItems().addAll(menuDeleteItem);
            list_view_user_medias.setContextMenu(contextMenu);
            list_view_user_medias.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    UIUtils.handleEmptyListCells(event,list_view_user_medias);
                    contextMenu.show(list_view_user_medias, event.getScreenX(), event.getScreenY());
                }
            });
        }
    }

    private void setUserListView() {
        users = FXCollections.observableArrayList(customConnection.getUsers());
        list_view_users.setItems(users);

        ContextMenu contextMenu = new ContextMenu();
        contextMenu.setStyle("-fx-background-color:#724f3f;" +
                "-fx-fill:white;" + "-fx-min-width:100px;" + "-fx-background-radius:50px;");

        MenuItem menuDeleteItem = new MenuItem();
        menuDeleteItem.setText("Delete");
        menuDeleteItem.setOnAction(event -> {
            User pickedUser =  users.get(list_view_users.getSelectionModel().getSelectedIndex());
            customConnection.deleteUser(pickedUser);
            list_view_users.getItems().remove(pickedUser);
            users = FXCollections.observableArrayList(customConnection.getUsers());
            contextMenu.hide();
        });

        contextMenu.getItems().addAll(menuDeleteItem);

        list_view_users.setCellFactory(param -> {
            return new ListCell<User>() {
                @Override
                protected void updateItem(User item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null && !empty) {

                        ImageView imageView = new ImageView(item.getPicture().toString());
                        imageView.setFitWidth(60);
                        imageView.setFitHeight(40);
                        imageView.preserveRatioProperty();
                        Label label = new Label(item.getName());
                        label.setTextFill(Color.WHITE);

                        this.setContextMenu(contextMenu);
                        HBox hBox = new HBox();
                        hBox.setSpacing(10);
                        hBox.getChildren().addAll(imageView, label);
                        hBox.setAlignment(Pos.CENTER_LEFT);
                        setGraphic(hBox);
                    }
                }
            };
        });
        list_view_users.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                user = (User) list_view_users.getSelectionModel().getSelectedItem();
                if(user != null){
                    displayUser(list_view_users.getSelectionModel().getSelectedIndex());
                    contextMenu.show(list_view_users, event.getScreenX(), event.getScreenY() + 20);
                }else{
                    UIUtils.handleEmptyListCells(event,list_view_users);
                    event.consume();
                }
            }
        });
    }

}
