package com.bmyguest.controller;

import com.bmyguest.entity.Event;
import com.bmyguest.entity.User;
import com.bmyguest.network.CustomConnection;
import com.bmyguest.tools.AppProperties;
import com.jfoenix.effects.JFXDepthManager;
import com.sun.deploy.net.HttpRequest;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Window;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Controller implements Initializable {
    @FXML
    private Button btn_login, btn_logout;

    @FXML
    private TextField editText_user_name;

    @FXML
    private TextField editText_user_password;

    @FXML
    private Label label_error;

    @FXML
    private AnchorPane cardPane;

    @FXML
    private ImageView logo_bmg;

    Window window;

    public void onLoginButtonPressed(ActionEvent event) {
        if (editText_user_name.getText().isEmpty() || editText_user_name == null
                || editText_user_password.getText().isEmpty() || editText_user_password.getText() == null) {
            label_error.setVisible(Boolean.TRUE);
            editText_user_name.setText(AppProperties.DEFAULT_LOGIN_EMAIL);
            editText_user_password.setText(AppProperties.DEAULT_LOGIN_PASSWORD);
        } else {
            /*
            //CustomConnection.insertUser(AppProperties.API_UR_SUFFIX_POST_EVENT);
            ProgressIndicator pi = new ProgressIndicator();
            VBox vBox = new VBox(pi);
            vBox.setAlignment(Pos.CENTER);
            StackPane stackPane = new StackPane(vBox);
            Scene scene = new Scene(stackPane);
            */

            window = ((Node) event.getSource()).getScene().getWindow();
            Parent manageParent = null;
            try {
                manageParent = FXMLLoader.load(getClass().getResource("../layout/new_main.fxml"));
                window.getScene().setRoot(manageParent);
            } catch (IOException e) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, String.valueOf(e));
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        editText_user_name.setFocusTraversable(false);
        JFXDepthManager.setDepth(cardPane, 200);
        JFXDepthManager.setDepth(logo_bmg, 30);
        label_error.setVisible(Boolean.FALSE);
    }

}
